# OCON

C++ distributed open source controller library.

Product page: https://oconomy.gitlab.io/page/

## Installation (Linux)

- Tested on Ubuntu 18.04 LTS and Raspbian GNU/Linux 9
- gcc 6.3.0 or higher
- CMake 3.10 or higher
- Boost 1.70.0 or higher (https://www.boost.org/users/history/version_1_70_0.html)

Required packages:

``` bash
[sudo] apt install build-essential pkg-config protobuf-compiler libprotobuf-dev libgflags-dev libgtest-dev
```
### Build and install OCON from source
``` bash
# Clone git repository
git clone https://gitlab.com/oconomy/ocon.git && cd ocon
# Build the project
mkdir -p cmake/build && cd cmake/build
cmake -DCMAKE_BUILD_TYPE=Release ../..  && make
# Install
[sudo] make install
```
## Examples
Examples include a single node [pendulum](examples/pendulum) and a multi node [bicycle controller](examples/bicycle).
## Run tests
Execute the tester from the cmake build directory:
``` bash
test/ocon_tester
```
## GUI
Work in progress. 

Simple (static) web gui for monitoring the node tree in real time (assumes websocket configured at port 8080)
The GUI can be run from any network location.
Nodes can be configured manually by editing the `gui/web/config.json` file. 

![](https://oconomy.gitlab.io/page/ocon-gui.png)

Requires AngularDart (see installation instructions [here](https://webdev.dartlang.org/tools/sdk#install))

```bash
cd gui
pub global activate webdev
webdev serve web:<port>
``` 