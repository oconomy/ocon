/*
 *
 * Copyright 2018 Peter de Lange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "utils.h"

#include <sstream>

namespace ocon {

using namespace google::protobuf;

bool RecursiveMessageSearch(const Message &message, const Message &candidate,
        std::vector<const FieldDescriptor*> &fields)
{
    if (&message == &candidate) return true; // Bingo!
    auto descriptor = candidate.GetDescriptor();
    for (int i = 0; i < descriptor->field_count(); i++) {
        auto field = descriptor->field(i);
        if (field->type() == FieldDescriptor::TYPE_MESSAGE && !field->is_repeated()) {
            fields.emplace_back(field);
            const Message& new_candidate = candidate.GetReflection()->GetMessage(candidate, field);
            if (RecursiveMessageSearch(message, new_candidate, fields)) {
                return true;
            } else {
                fields.pop_back();
            }
        }
    }
    return false;
}

void AllocateMessages(Message* message)
{
    auto descriptor = message->GetDescriptor();
    for (int i = 0; i < descriptor->field_count(); i++) {
        auto field = descriptor->field(i);
        if (field->type() == FieldDescriptor::TYPE_MESSAGE && !field->is_repeated()) {
            AllocateMessages(message->GetReflection()->MutableMessage(message, field));
        }
    }
}


std::string CreateMessagePathFromFields(const std::vector<const google::protobuf::FieldDescriptor*> &fields)
{
    if (fields.empty()) return "";
    std::stringstream ss;
    ss << fields[0]->name();
    for (unsigned i = 1; i < fields.size(); i++) {
        ss << "." << fields[i]->name();
    }
    return ss.str();
}


Message *MessageFieldResolver(Message *message, const std::vector<const FieldDescriptor *> &fields, unsigned index)
{
    if (fields.size() == index) {
        return message;
    }
    auto &field = fields[index];
    if (field->type() == FieldDescriptor::TYPE_MESSAGE && !field->is_repeated()) {
        auto mutable_message = message->GetReflection()->MutableMessage(message, field);
        return MessageFieldResolver(mutable_message, fields, index + 1);
    } else {
        return message;
    }
}

bool GenerateFieldsFromPath(const Message* message, const std::string& path,
        std::vector<const FieldDescriptor*>& fields)
{
    if (message == nullptr) return false;
    if (path.empty()) return true;
    auto index = path.find_first_of('.');
    const google::protobuf::FieldDescriptor* field;
    if (index > path.size()) {
        field = message->GetDescriptor()->FindFieldByName(path);
        if (field != nullptr) {
            fields.push_back(field);
            return true;
        }
    } else {
        field = message->GetDescriptor()->FindFieldByName(path.substr(0, index));
        if (field != nullptr) {
            if (!field->is_repeated()) {
                fields.push_back(field);
                return GenerateFieldsFromPath(&message->GetReflection()->
                        GetMessage(*message, field), path.substr(index + 1, path.size()), fields);
            }
        }
    }
    return false; // field not found
}

} // namespace ocon
