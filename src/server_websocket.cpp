/*
 *
 * Copyright 2018 Peter de Lange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "server_websocket.h"

#include <ocon/node.h>

#include <boost/asio/bind_executor.hpp>
#include <google/protobuf/util/json_util.h>

#include <iostream>

namespace ocon {

ServerWebSocket::ServerWebSocket(const method_t_& method, const ServerConfiguration& config) :
        address_(boost::asio::ip::make_address_v4(config.host())),
        port_(static_cast<const unsigned short>(config.port_websocket())),
        ioc_{number_of_threads_},
        connections_(0), method_(method) {
}

ServerWebSocket::~ServerWebSocket() {
    ioc_.stop();
    for (auto& thread : threads_) {
        thread.join();
    }
}

void ServerWebSocket::Run() {

    // Create and launch a listening port
    std::make_shared<listener>(ioc_, boost::asio::ip::tcp::endpoint{address_, port_}, &method_, connections_)->run();
    // Run the I/O service on the requested number of threads
    for (auto i = 0 ; i < number_of_threads_; i++) {
        threads_.emplace_back([&] {
            ioc_.run();
        });
    }
    std::cout << "OCON websocket server listening on " <<  boost::asio::ip::tcp::endpoint{address_, port_} << std::endl;
}

int ServerWebSocket::number_of_open_connections() {
    return connections_;
}

void ServerWebSocket::fail(boost::system::error_code ec, char const *what) {
    std::cerr << what << ": " << ec.message() << "\n";
}

    ServerWebSocket::listener::listener(boost::asio::io_context &ioc, boost::asio::ip::tcp::endpoint endpoint,
            method_t_* method, std::atomic_int& connections)
        : acceptor_(ioc), ioc_(ioc), method_(method), connections_(connections) {

    boost::system::error_code ec;

    // Open the acceptor
    acceptor_.open(endpoint.protocol(), ec);
    if (ec) {fail(ec, "open"); return;}

    // Allow address reuse
    acceptor_.set_option(boost::asio::socket_base::reuse_address(true), ec);
    if (ec) {fail(ec, "set_option"); return;}

    // Bind to the server address
    acceptor_.bind(endpoint, ec);
    if (ec) {fail(ec, "bind"); return;}

    // Start listening for connections
    acceptor_.listen(boost::asio::socket_base::max_listen_connections, ec);
    if (ec) {fail(ec, "listen"); return;}
}

void ServerWebSocket::listener::run() {
    if (!acceptor_.is_open()) return;
    do_accept();
}

void ServerWebSocket::listener::do_accept() {
    acceptor_.async_accept(boost::beast::net::make_strand(ioc_),
            boost::beast::bind_front_handler(&listener::on_accept, shared_from_this()));
}

void ServerWebSocket::listener::on_accept(boost::system::error_code ec, boost::asio::ip::tcp::socket socket) {
    if (ec) {
        fail(ec, "accept");
    } else {
        // Create the session and run it
        std::make_shared<session>(std::move(socket), method_, connections_)->run();
    }
    // Accept another connection
    do_accept();
}

ServerWebSocket::session::session(boost::asio::ip::tcp::socket socket, method_t_* method,
        std::atomic_int& connections) :
        ws_(std::move(socket)),  method_(method), connections_(connections) {
    connections_++;
}

ServerWebSocket::session::~session() {
    connections_--;
}

void ServerWebSocket::session::run() {
    // Accept the websocket handshake
    ws_.async_accept(boost::beast::bind_front_handler(&session::on_accept, shared_from_this()));
}

void ServerWebSocket::session::on_accept(boost::system::error_code ec) {
    if (ec) return fail(ec, "accept");
    // Read a message
    do_read();
}

void ServerWebSocket::session::do_read() {
    // Read a message into our buffer
    ws_.async_read(request_buffer_, boost::beast::bind_front_handler(&session::on_read, shared_from_this()));
}

void ServerWebSocket::session::on_read(boost::system::error_code ec, std::size_t bytes_transferred) {
    boost::ignore_unused(bytes_transferred);

    // This indicates that the session was closed
    if (ec == boost::beast::websocket::error::closed) return;
    if (ec) fail(ec, "read");

    // This is were the magic happens
    std::string request = boost::beast::buffers_to_string(request_buffer_.data());
    std::string response;
    bool result = (*method_)(request, response);
    if (!result) {
        return;
    };

    // write the response
    ws_.text(ws_.got_text());
    ws_.async_write(boost::asio::buffer(response), boost::beast::bind_front_handler(&session::on_write, shared_from_this()));
}

void ServerWebSocket::session::on_write(boost::system::error_code ec, std::size_t bytes_transferred) {
    boost::ignore_unused(bytes_transferred);

    if (ec) return fail(ec, "write");

    // Clear the buffer
    request_buffer_.consume(request_buffer_.size());

    // Do another read
    do_read();
}

} // namespace ocon
