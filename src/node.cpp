/*
 *
 * Copyright 2018 Peter de Lange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <ocon/node.h>

#include "utils.h"

#include <functional>
#include <algorithm>
#include <fstream>
#include <sstream>

namespace ocon {

using namespace google::protobuf;


// Root node constructor
Node::Node(Node* parent, const std::string& name, const unsigned& multi_rate_factor) :
        parent_(parent), name_(name), multi_rate_factor_(multi_rate_factor)
{
    // Node info:
    info_.set_name(name);
    info_.set_multi_rate_factor(multi_rate_factor);
    if (parent == nullptr) {
        // Root node is responsible for creating these pointers
        node_map_ = std::make_shared<node_map_t_>();
    } else {
        // Child node inherits shared pointers from parent
        node_map_ = parent->node_map_;
        parent->children_.emplace_back(this);
    }
    // Insert in node tree map and throw on duplicate
    if (!node_map_->insert(std::make_pair(path(), this)).second) {
        throw std::runtime_error("Duplicate child '" + path() + "' found in node tree");
    };
}

void Node::ExecuteInit()
{
    // First pass of initialize
    if (!info_.initialized()) {
        std::lock_guard<std::mutex> guard(mu_info_);
        info_.set_type(typeid(*this).name());
        info_.set_initialized(true);
    }
    // Update all bindings owned by current node
    ExecuteSignalBindings(bindings_);
    // Call virtual init method.
    Init();
    // Execute init algorithm for all children
    std::for_each(children_.begin(), children_.end(),[](Node* child){
        child->ExecuteInit();
    });
    // Update callback bindings: inputs which are coupled to descendant outputs
    ExecuteSignalBindings(bindings_callback_);
    // Call virtual callback method.
    InitCallback();
}

void Node::ExecuteStep(const double& time_step)
{
    // Update all bindings owned by current node
    ExecuteSignalBindings(bindings_);
    // Execute step at a multiple of the parent frequency (default = 1)
    const double time_step_multi_rate = time_step / multi_rate_factor_;
    {
        std::lock_guard<std::mutex> guard(mu_info_);
        info_.set_time_step(time_step_multi_rate);
    }
    for (unsigned i = 0; i < multi_rate_factor_; i++) {
        // Call virtual step method.
        Step(time_step_multi_rate);
        // Execute step algorithm for all children
        std::for_each(children_.begin(), children_.end(), [&time_step_multi_rate](Node* child){
            child->ExecuteStep(time_step_multi_rate);
        });
        // Update callback bindings: inputs which are coupled to descendant outputs
        ExecuteSignalBindings(bindings_callback_);
    }
    // Call virtual callback method.
    StepCallback();
}

void  Node::ExecuteSignalBindings(const bindings_t_& bindings)
{
    for (const auto& binding : bindings) {
        // locking order matters to avoid dead lock.
        std::lock_guard<std::mutex> guard1(binding.lock_order.first->mu_);
        std::lock_guard<std::mutex> guard2(binding.lock_order.second->mu_);
        auto target_message = MessageFieldResolver(binding.target_interface->message, binding.target_fields);
        auto source_message = MessageFieldResolver(binding.source_interface->message, binding.source_fields);
        // execute the copy
        target_message->CopyFrom(*source_message);
    }
}

void Node::RegisterInterface(const std::string &name, Message* interface, NodeInterfaceType type)
{
    if (info_.initialized()) {
        throw std::runtime_error("Node already initialized: register interfaces before calling ExecuteInit()");
    }
    if (FindInterface(name) != nullptr) {
        throw std::runtime_error("Attempting to register duplicate interface with name: " + name);
    }
    interfaces_.emplace_back(Interface{name, interface, type});
    AllocateMessages(interface);
}

NodeInfo Node::info() const
{
    NodeInfo result;
    std::lock_guard<std::mutex> guard(mu_info_);
    result.CopyFrom(info_);
    return info_;
}

NodeTree Node::tree() const {
    NodeTree result;
    result.set_path(path());
    {
        std::lock_guard<std::mutex> guard(mu_info_);
        result.mutable_info()->CopyFrom(info_);
    }
    for (const auto& binding : bindings_) {
        result.add_bindings()->CopyFrom(binding.message);
    }
    for (const auto& interface : interfaces_) {
        auto added_interface = result.add_interfaces();
        added_interface->set_name(interface.name);
        added_interface->set_type(interface.type);
        if (interface.message != nullptr) {
            std::lock_guard<std::mutex> guard(mu_);
            added_interface->mutable_message()->PackFrom(*interface.message);
        }
    }
    for (const auto& child : children_) {
        result.add_children()->CopyFrom(child->tree());
    }
    return result;
}

Any Node::message(const NodeMessageReference& request) const
{
    Any result;
    std::lock_guard<std::mutex>guard(mu_);
    auto parse_result = ParseMessageRequest(request);
    if (parse_result.success) {
        result.PackFrom(*parse_result.message);
    }
    return result;
}

std::string Node::path() const
{
    if (parent_ != nullptr) {
        return parent_->path() + '.' + name_;
    } else {
        return name_;
    }
}

void Node::ConnectSignalsImpl(const Message& source_message, const Message& target_message, bool optimize)
{
    if (info_.initialized()) {
        throw std::runtime_error("Node already initialized: connect signals before calling ExecuteInit()");
    }

    auto source = SearchMessage(source_message);
    auto target = SearchMessage(target_message);

    SignalBinding message;
    message.mutable_source()->CopyFrom(source.reference);
    message.mutable_target()->CopyFrom(target.reference);

    // Some more checks
    if (source.node == target.node) {
        throw std::runtime_error("Binding error: binding source node equals target node. " + message.DebugString());
    }

    // Check source interface type
    std::vector<NodeInterfaceType> source_disallowed_types{INTERNAL, PARAMETER};
    for (auto& type: source_disallowed_types) {
        if (type == source.interface->type) {
            const auto &name = NodeInterfaceType_Name(type);
            throw std::runtime_error("Binding error: source type " + name + " not allowed " + message.DebugString());
        }
    }

    // Check target interface type
    std::vector<NodeInterfaceType> target_disallowed_types{OUTPUT, INTERNAL, PARAMETER };
    for (auto& type: target_disallowed_types) {
        if (type == target.interface->type) {
            const auto &name = NodeInterfaceType_Name(type);
            throw std::runtime_error("Binding error: target type " + name + " not allowed " + message.DebugString());
        }
    }

    // set locking order, e.g. which node should locks first.
    typedef std::pair<const Node*, const Node*> node_pair_t;
    std::function<bool (const Node*, node_pair_t&)> lock_order = [&] (const Node* node, node_pair_t& result) {
        if (node == target.node) {
            result = std::make_pair(target.node, source.node);
            return true;
        }
        if (node == source.node) {
            result = std::make_pair(source.node, target.node);
            return true;
        }
        for (auto& child : node->children_) {
            if (lock_order(child, result)) return true;
        }
        if (node == GetRoot()) {
            throw std::runtime_error("Binding error: unexpected error when setting lock order.");
        }
        return false;
    };

    // Create signal binding
    SignalBindingData binding;
    binding.message = message;
    lock_order(GetRoot(), binding.lock_order);
    binding.source_interface = source.interface;
    binding.target_interface = target.interface;
    binding.source_fields = source.fields;
    binding.target_fields = target.fields;

    // Determine owner of the signal binding. By default (optimize == true) the target owns the binding
    // and thus the bindings are executed just in time (e.g. minimal latency).
    // If false, the current node owns the binding.
    Node* owner = optimize ? target.node : this;

    // Insert regular binding
    owner->bindings_.emplace_back(binding);

    // Insert callback binding if optimization is enabled and the target is an ancestor of source node
    if (optimize) {
        auto source_node_parent = source.node->parent_;
        while (source_node_parent != nullptr) {
            if (source_node_parent == target.node) {
                owner->bindings_callback_.emplace_back(binding);
                break;
            }
            source_node_parent = source_node_parent->parent_;
        }
    }
}

Node::ParseMessageRequestResult Node::ParseMessageRequest(const NodeMessageReference& request) const
{
    // prepare result, assume success unless error is detected.
    ParseMessageRequestResult result;

    // return error
    auto error = [&result](const std::string& error_string) {
        result.error = error_string;
        return result;
    };

    // Retrieve the node and parse the provided message path
    if (node_map_->find(request.node_path()) == node_map_->end()) {
        return error("Node not found");
    }
    result.node = node_map_->at(request.node_path());

    // Retrieve the node interface key
    const auto &path = request.message_path();
    auto index = path.find_first_of('.');
    result.key = path.substr(0, std::min(index, path.size()));

    // Find the specified node
    auto interface = FindInterface(result.key);
    if (interface == nullptr) {
        return error("Could not resolve interface key");
    }

    auto message_path = path.substr(std::min(index + 1, path.size()), path.size());
    if (!GenerateFieldsFromPath(interface->message, message_path, result.fields)) {
        return error("Could not resolve message path");
    }

    // Return the requested message by resolving the fields
    result.message = MessageFieldResolver(interface->message, result.fields);
    result.success = true;
    return result;
}

Node::SearchMessageResult Node::SearchMessage(const Message& message) const
{
    SearchMessageResult result;

    // scan through node map search all interfaces recursively
    for (auto& node_map_item : *node_map_) {
        auto& node = node_map_item.second;
        for (auto& interface : node->interfaces_) {
            if (interface.message != nullptr) {
                if (RecursiveMessageSearch(message, *interface.message, result.fields)) {
                    result.node = node;
                    result.interface = &interface;
                    result.reference = CreateNodeMessageReference(node, interface.name, result.fields);
                    return result;
                };
            }
        }
    }
    // throw exception in case of no result
    std::stringstream ss;
    ss << "Could not find message in provided node map at address: " << &message;
    throw std::runtime_error(ss.str());
}

const Node::Interface* Node::FindInterface(const std::string &name) const {
    for (auto &interface: interfaces_) {
        if (name == interface.name) return &interface;
    }
    return nullptr;
}

const Node *Node::GetRoot() const {
    if (parent_ == nullptr) {
        return this;
    } else {
        return parent_->GetRoot();
    }
}

NodeMessageReference Node::CreateNodeMessageReference(const ocon::Node *node,
        const std::string& interface_name, const fields_t_ &fields)
{
    NodeMessageReference result = {};
    result.set_node_path(node->path());
    if (!fields.empty()) {
        result.set_message_path(interface_name + "." + CreateMessagePathFromFields(fields));
    } else{
        result.set_message_path(interface_name);
    }
    return result;
}

} // namespace ocon
