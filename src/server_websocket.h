/*
 *
 * Copyright 2018 Peter de Lange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

#include <ocon/server.pb.h>

#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/strand.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/websocket.hpp>

#include <thread>

namespace ocon {

class ServerWebSocket {

    const boost::asio::ip::address address_;
    const unsigned short port_;
    const int number_of_threads_ = 1;
    std::vector<std::thread> threads_;
    boost::asio::io_context ioc_;

    std::atomic_int connections_;

    typedef std::function<bool(const std::string&  /* request */, std::string& /* response */)> method_t_;
    method_t_ method_;

public:
    ServerWebSocket(const method_t_& method, const ServerConfiguration& config);
    ~ServerWebSocket();

    void Run();
    int number_of_open_connections();

private:
    // Report a failure
    static void fail(boost::system::error_code ec, char const *what);
    // Accepts incoming connections and launches the sessions
    class listener : public std::enable_shared_from_this<listener> {
        boost::asio::ip::tcp::acceptor acceptor_;
        boost::beast::net::io_context& ioc_;
        method_t_* method_;
        std::atomic_int& connections_;

    public:
        listener(boost::asio::io_context &ioc, boost::asio::ip::tcp::endpoint endpoint, method_t_* method_,
                std::atomic_int& connections);

        // Start accepting incoming connections
        void run();
        void do_accept();
        void on_accept(boost::system::error_code ec, boost::asio::ip::tcp::socket socket);
    };

    class session : public std::enable_shared_from_this<session> {
        boost::beast::websocket::stream<boost::asio::ip::tcp::socket> ws_;
        boost::beast::multi_buffer request_buffer_;
        method_t_* method_;
        std::atomic_int& connections_;
    public:
        // Take ownership of the socket
        session(boost::asio::ip::tcp::socket socket, method_t_* method, std::atomic_int& connections);
        ~session();;
        // Start the asynchronous operation
        void run();
        void on_accept(boost::system::error_code ec);
        void do_read();
        void on_read(boost::system::error_code ec, std::size_t bytes_transferred);
        void on_write(boost::system::error_code ec, std::size_t bytes_transferred);
    };
};

}  // namespace ocon
