/*
 *
 * Copyright 2018 Peter de Lange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "test_nodes.h"

#include <google/protobuf/util/message_differencer.h>
#include <gtest/gtest.h>

#include <thread>
#include <chrono>
#include <fstream>
#include <atomic>

using namespace ocon::test;

TEST(ControllerTest, TestControllerData) {
    
    ControllerTestFixture f;
    f.ExecuteInit();
    double time_step = 0.01;
    f.ExecuteStep(time_step);

    // check whether multi rate data members are initialized accordingly
    EXPECT_EQ(f.info().time_step(), time_step / f.multi_rate_factor_);
    EXPECT_EQ(f.info().multi_rate_factor(), f.multi_rate_factor_);
    EXPECT_EQ(f.info().name(), "TestFixture");
    EXPECT_TRUE(f.info().type().find("ControllerTestFixture") < f.info().type().length());
}

TEST(ControllerTest, TestControllerHierarchy) {
    
    ControllerTestFixture f;

    google::protobuf::util::MessageDifferencer message_differencer;
    
    auto root_node = f.tree();
    EXPECT_EQ(root_node.info().name(), "TestFixture");
    EXPECT_EQ(root_node.path(), "TestFixture");
    EXPECT_TRUE(message_differencer.Compare(root_node.children(0), f.data_types_.tree()));
    EXPECT_EQ(root_node.children(0).interfaces(0).name(), "inputs");
    EXPECT_EQ(root_node.children(0).interfaces(1).name(), "outputs");
    EXPECT_EQ(root_node.children(0).interfaces(2).name(), "internals");
    EXPECT_EQ(root_node.children(0).interfaces(3).name(), "parameters");
    const auto controller1_node = f.controller1_.tree();
    EXPECT_EQ(controller1_node.info().name(), "Controller1");
    EXPECT_EQ(controller1_node.path(), "TestFixture.Controller1");
    EXPECT_TRUE(message_differencer.Compare(root_node.children(1), f.controller1_.tree()));
    const auto controller2_node = f.controller2_.tree();
    EXPECT_EQ(controller2_node.info().name(), "Controller2");
    EXPECT_EQ(controller2_node.path(), "TestFixture.Controller2");
    EXPECT_TRUE(message_differencer.Compare(root_node.children(2), f.controller2_.tree()));
    const auto controller3_node = f.controller3_.tree();
    EXPECT_EQ(controller3_node.info().name(), "Controller3");
    EXPECT_EQ(controller3_node.path(), "TestFixture.Controller2.Controller3");

}

TEST(ControllerTest, TestIterationsAndTimeStep) {

    ControllerTestFixture f;
    f.ExecuteInit();
    double time_step = 0.01;

    // check whether multi rate data members are initialized accordingly
    EXPECT_EQ(f.info().multi_rate_factor(), f.multi_rate_factor_);
    EXPECT_EQ(f.controller1_.info().multi_rate_factor(), f.multi_rate_factor_1_);
    EXPECT_EQ(f.controller2_.info().multi_rate_factor(), f.multi_rate_factor_2_);
    EXPECT_EQ(f.controller3_.info().multi_rate_factor(), f.multi_rate_factor_3_);

    // check iterations and time of sub models
    EXPECT_EQ(f.controller1_.outputs().iterations(), 0);
    EXPECT_EQ(f.controller2_.outputs().iterations(), 0);
    EXPECT_EQ(f.controller3_.outputs().iterations(), 0);
    ASSERT_DOUBLE_EQ(f.controller1_.outputs().time(), 0.);
    ASSERT_DOUBLE_EQ(f.controller2_.outputs().time(), 0.);
    ASSERT_DOUBLE_EQ(f.controller3_.outputs().time(), 0.);

    // perform update and check iterations and time
    f.ExecuteStep(time_step);
    EXPECT_EQ(f.controller1_.outputs().iterations(), f.multi_rate_factor_ * f.multi_rate_factor_1_);
    EXPECT_EQ(f.controller2_.outputs().iterations(), f.multi_rate_factor_ * f.multi_rate_factor_2_);
    EXPECT_EQ(f.controller3_.outputs().iterations(), f.multi_rate_factor_ * f.multi_rate_factor_2_ * f.multi_rate_factor_3_);
    EXPECT_NEAR(f.multi_rate_factor_ * f.info().time_step(), time_step, 1e-9);
    EXPECT_NEAR(f.controller1_.outputs().time(), time_step, 1e-9);
    EXPECT_NEAR(f.controller2_.outputs().time(), time_step, 1e-9);
    EXPECT_NEAR(f.controller3_.outputs().time(), time_step, 1e-9);

}


TEST(ControllerTest, OptimalSignalBinding) {

    ControllerTestDaisyChain f;
    f.ExecuteInit();

    // test (optimal) signal binding.
    ASSERT_FALSE(f.inputs().test_bool_message().test());
    ASSERT_FALSE(f.controller1_.inputs().test_bool_message().test());
    ASSERT_FALSE(f.controller2_.inputs().test_bool_message().test());
    ASSERT_FALSE(f.controller3_.inputs().test_bool_message().test());
    ASSERT_FALSE(f.outputs().test_bool_message().test());
    ASSERT_FALSE(f.controller1_.outputs().test_bool_message().test());
    ASSERT_FALSE(f.controller2_.outputs().test_bool_message().test());
    ASSERT_FALSE(f.controller3_.outputs().test_bool_message().test());

    f.mutable_inputs()->mutable_test_bool_message()->set_test(true);

    f.ExecuteStep(0.01);

    ASSERT_TRUE(f.inputs().test_bool_message().test());
    ASSERT_TRUE(f.controller1_.inputs().test_bool_message().test());
    ASSERT_TRUE(f.controller2_.inputs().test_bool_message().test());
    ASSERT_TRUE(f.controller3_.inputs().test_bool_message().test());
    ASSERT_TRUE(f.outputs().test_bool_message().test());
    ASSERT_TRUE(f.controller1_.outputs().test_bool_message().test());
    ASSERT_TRUE(f.controller2_.outputs().test_bool_message().test());
    ASSERT_TRUE(f.controller3_.outputs().test_bool_message().test());

}

TEST(ControllerTest, TestConnectSignals) {

    // Tests are located in test fixture
    TestConnectSignals f;
    // Call execute init
    ASSERT_NO_THROW(f.ExecuteInit());
    // Check if all is well on binding execution
    ASSERT_NO_THROW(f.ExecuteStep(0.01));

}

TEST(ControllerTest, TestInitialize) {

    // Tests are located in test fixture
    ControllerDummy f(nullptr, "Test");
    // Call execute init
    ASSERT_NO_THROW(f.ExecuteInit());
    // Check if all is well on binding execution
    ASSERT_NO_THROW(f.ExecuteStep(0.01));
}

TEST(ControllerTest, TestMutexLocking) {

    MutexTestRoot root;
    root.ExecuteInit();

    std::atomic_bool running(true);

    // Step root model continuously for 1 second
    auto t_update = std::thread([&]() {
        auto time_to_stop = std::chrono::steady_clock::now() + std::chrono::seconds(1);
        while (std::chrono::steady_clock::now() < time_to_stop) {
            root.ExecuteStep(0.01);
        }
        running = false;
    });

    // Interrupt at prime intervals to simulate some irregularity from multiple threads
    const unsigned number_of_threads = 10;
    std::vector<std::thread> threads;
    for (unsigned _ = 0; _ < number_of_threads; _++) {
        threads.emplace_back(std::thread([&]() {
            int i = 0;
            while (running) {
                if (i % 2 == 0) root.Interrupt();
                if (i % 13 == 0) root.child_0.Interrupt();
                if (i % 3 == 0) root.child_1.Interrupt();
                if (i % 5 == 0) root.child_0_0.Interrupt();
                if (i % 7 == 0) root.child_0_1.Interrupt();
                if (i % 11 == 0) root.tree();
            }
        }));
    }

    t_update.join();
    for (auto& thread : threads) thread.join();
}
