/*
 *
 * Copyright 2018 Peter de Lange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "test_nodes.h"

#include "../src/server_websocket.h"

#include <ocon/server.h>

#include <boost/beast/core.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <gtest/gtest.h>
#include <google/protobuf/util/message_differencer.h>

#include <string>

using namespace ocon::test;

class ServerTestFixture {

public:

    ocon::ServerConfiguration config_;

    ControllerDummy* node_ = nullptr;
    ocon::Server* server_ = nullptr;
    google::protobuf::util::MessageDifferencer message_differencer_;

    explicit ServerTestFixture() {
        config_.set_port_websocket(8080);
        node_ = new ControllerDummy(nullptr, "Dummy");
        server_ = new ocon::Server(node_, config_);
        server_->Run();
    }

    ~ServerTestFixture() {
        delete server_;
        delete node_;
    }

    ocon::ServerConfiguration server_config() {
        return server_->config();
    }
};


class WebSocketClient {
    // The io_context is required for all I/O
    boost::asio::io_context ioc_;
    boost::beast::websocket::stream<boost::asio::ip::tcp::socket> ws_;
public:
    explicit WebSocketClient(const ocon::ServerConfiguration &config) : ws_{ioc_}
    {
        boost::asio::ip::tcp::resolver resolver{ioc_};
        auto const results = resolver.resolve(config.host(), std::to_string(config.port_websocket()));
        boost::asio::connect(ws_.next_layer(), results.begin(), results.end());
        ws_.handshake(config.host(), "/");
    }

    ~WebSocketClient() {
        ws_.close(boost::beast::websocket::close_code::normal);
    }

    std::string WriteAndRead(const std::string &message) {
        ws_.write(boost::asio::buffer(message));
        boost::beast::multi_buffer buffer;
        ws_.read(buffer);
        return boost::beast::buffers_to_string(buffer.data());
    }
};


// Sends a WebSocket message and prints the response
TEST(ControllerTest, TestServerWebSocket) {

    // Dummy echo method
    auto method = [](const std::string& request, std::string& response) -> bool {
        response = request;
        return true;
    };

    ocon::ServerConfiguration config;
    config.set_host("127.0.0.1");
    config.set_port_websocket(8081);
    ocon::ServerWebSocket fixture(method, config);
    fixture.Run();

    // Client
    WebSocketClient client(config);
    std::string request = "Hi Mark!";
    std::string response = client.WriteAndRead(request);

    EXPECT_EQ(request, response);
}


TEST(ControllerTest, NodeServerConfigDefaultValues) {

    ControllerDummy model (nullptr, "Model");

    // Empty config should give default values
    ocon::ServerConfiguration config;
    ocon::Server server_default (&model, config);
    EXPECT_EQ(server_default.config().host(), "127.0.0.1");
    EXPECT_EQ(server_default.config().port_websocket(), 8080);
    EXPECT_EQ(server_default.config().port_grpc(), 50051);

    // Partial config should give default values except the specified parameters
    config.set_port_grpc(1);
    ocon::Server server_partial(&model, config);
    EXPECT_EQ(server_default.config().host(), "127.0.0.1");
    EXPECT_EQ(server_partial.config().port_websocket(), 8080);
    EXPECT_EQ(server_partial.config().port_grpc(), 1);
}
