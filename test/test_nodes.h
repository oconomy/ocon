/*
 *
 * Copyright 2018 Peter de Lange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "test/messages.pb.h"

#include <ocon/node.h>
#include <ocon/node_template.h>

#include <gtest/gtest.h>
#include "gmock/gmock.h"

#include <chrono>

namespace ocon {
namespace test {

class MutexTest : public NodeTemplate<TestMessageBar, TestMessageBar,
        google::protobuf::Empty, google::protobuf::Empty> {
public:
    MutexTest(Node *parent, const std::string &name) : NodeTemplate(parent, name) {}

    void Step(const double &time_step) override {
        std::lock_guard<std::mutex> guard(mu_);
        outputs_->set_bar(outputs_->bar() + 1);
    }

    void StepCallback() override {
        std::lock_guard<std::mutex> guard(mu_);
        outputs_->set_bar(outputs_->bar() + 1);
    }

    void Interrupt() {
        std::lock_guard<std::mutex> guard(mu_);
        outputs_->set_bar(outputs_->bar() + 1);
    }
};

class MutexTestRoot : public MutexTest {
public:
    MutexTest child_0;
    MutexTest child_1;
    MutexTest child_0_0;
    MutexTest child_0_1;

    MutexTestRoot() : MutexTest(nullptr, "MutexTestRoot"),
                      child_0(this, "child_0"),
                      child_1(this, "child_1"),
                      child_0_0(&child_0, "child_0_0"),
                      child_0_1(&child_0, "child_0_1") {
        ConnectSignals(outputs(), child_0.inputs());
        ConnectSignals(outputs(), child_1.inputs());
        ConnectSignals(outputs(), child_0_0.inputs());
        ConnectSignals(outputs(), child_0_1.inputs());
        ConnectSignals(child_0.outputs(), inputs());
        ConnectSignals(child_0.outputs(), child_1.inputs());
        ConnectSignals(child_0.outputs(), child_0_0.inputs());
        ConnectSignals(child_0.outputs(), child_0_1.inputs());
        ConnectSignals(child_1.outputs(), inputs());
        ConnectSignals(child_1.outputs(), child_0.inputs());
        ConnectSignals(child_1.outputs(), child_0_0.inputs());
        ConnectSignals(child_1.outputs(), child_0_1.inputs());
        ConnectSignals(child_0_0.outputs(), inputs());
        ConnectSignals(child_0_0.outputs(), child_0.inputs());
        ConnectSignals(child_0_0.outputs(), child_1.inputs());
        ConnectSignals(child_0_0.outputs(), child_0_1.inputs());
        ConnectSignals(child_0_1.outputs(), inputs());
        ConnectSignals(child_0_1.outputs(), child_0.inputs());
        ConnectSignals(child_0_1.outputs(), child_1.inputs());
        ConnectSignals(child_0_1.outputs(), child_0_0.inputs());
    }
};

struct TestConnectSignals : Node {

    struct TestNode : public Node {
        TestDataTypes inputs_;
        TestDataTypes outputs_;
        TestDataTypes internals_;
        TestDataTypes parameters_;
        TestDataTypes undefined_;
        TestDataTypes custom_; // should be undefined
        TestNode(Node *parent, const std::string &name) : Node(parent, name) {
            RegisterInterface("inputs", &inputs_, INPUT);
            RegisterInterface("outputs", &outputs_, OUTPUT);
            RegisterInterface("internals", &internals_, INTERNAL);
            RegisterInterface("parameters", &parameters_, PARAMETER);
            RegisterInterface("undefined", &undefined_, UNDEFINED);
            RegisterInterface("custom", &custom_); // Should be same as undefined.
        }
    };

    TestNode source_;
    TestNode target_;

    TestConnectSignals() : Node(nullptr, "TestConnectSignals", 1), source_(this, "Source"), target_(this, "Target") {
        // Allow only binding from undefined, inputs, and outputs, to inputs and undefined interface types
        EXPECT_NO_THROW(ConnectSignals(source_.outputs_, target_.inputs_));
        EXPECT_NO_THROW(ConnectSignals(source_.outputs_, target_.undefined_));
        EXPECT_NO_THROW(ConnectSignals(source_.outputs_, target_.custom_));
        EXPECT_NO_THROW(ConnectSignals(source_.inputs_, target_.inputs_));
        EXPECT_NO_THROW(ConnectSignals(source_.inputs_, target_.undefined_));
        EXPECT_NO_THROW(ConnectSignals(source_.inputs_, target_.custom_));
        EXPECT_NO_THROW(ConnectSignals(source_.undefined_, target_.inputs_));
        EXPECT_NO_THROW(ConnectSignals(source_.undefined_, target_.undefined_));
        EXPECT_NO_THROW(ConnectSignals(source_.undefined_, target_.custom_));
        EXPECT_NO_THROW(ConnectSignals(source_.custom_, target_.inputs_));
        EXPECT_NO_THROW(ConnectSignals(source_.custom_, target_.undefined_));
        EXPECT_NO_THROW(ConnectSignals(source_.custom_, target_.custom_));

        // Disallow binding to outputs, internals, parameters
        EXPECT_THROW(ConnectSignals(source_.outputs_, target_.outputs_), std::runtime_error);
        EXPECT_THROW(ConnectSignals(source_.outputs_, target_.internals_), std::runtime_error);
        EXPECT_THROW(ConnectSignals(source_.outputs_, target_.parameters_), std::runtime_error);

        // Disallow source signal to be of type parameters and internal
        EXPECT_THROW(ConnectSignals(source_.parameters_, target_.inputs_), std::runtime_error);
        EXPECT_THROW(ConnectSignals(source_.internals_, target_.inputs_), std::runtime_error);

        // Don't allow to bind messages within the same controller
        EXPECT_THROW(ConnectSignals(source_.outputs_, source_.inputs_), std::runtime_error);
        EXPECT_THROW(ConnectSignals(target_.outputs_, target_.inputs_), std::runtime_error);

        // Don't allow connect signals after initialize method is called
        ExecuteInit();
        EXPECT_THROW(ConnectSignals(source_.outputs_, target_.inputs_), std::runtime_error);
    }
};

class ControllerDummy : public NodeTemplate<TestDummyInputs, TestDummyOutputs, google::protobuf::Empty,
        TestDataTypes> {
public:
    explicit ControllerDummy(Node *parent, const std::string &name, unsigned multi_rate_factor = 1);

    explicit ControllerDummy(const std::string &name, unsigned multi_rate_factor = 1);

    TestDataTypes *mutable_parameters() { return parameters_; };

    // Override all method to test node info
    void Init() override {};

    void Step(const double &time_step) override;

    void InitCallback() override {};

    void StepCallback() override {};
};

class TestDataTypesUpdate : public NodeTemplate<TestDataTypes, TestDataTypes, google::protobuf::Empty,
        google::protobuf::Empty> {
public:
    explicit TestDataTypesUpdate(Node *parent, const std::string &name);

    void Step(const double &time_step) override;
};

class TestExchangeDataTypes : public NodeTemplate<TestDataTypes, TestDataTypes,
        google::protobuf::Empty, google::protobuf::Empty> {
public:
    TestDataTypesUpdate c1;
    TestDataTypesUpdate c2;

    explicit TestExchangeDataTypes(Node *parent, const std::string &name);
};

class ControllerTestDaisyChain : public NodeTemplate<TestDummyInputs, TestDummyOutputs, google::protobuf::Empty, google::protobuf::Empty> {
public:

    ControllerDummy controller1_;
    ControllerDummy controller2_;
    ControllerDummy controller3_;

    ControllerTestDaisyChain() :
            NodeTemplate(nullptr, "DaisyChain"),
            controller1_(this, "C1"), controller2_(this, "C2"), controller3_(this, "C3") {
        ConnectSignals(outputs().test_bool_message(), controller1_.inputs().test_bool_message());
        ConnectSignals(controller1_.outputs().test_bool_message(), controller2_.inputs().test_bool_message());
        ConnectSignals(controller2_.outputs().test_bool_message(), controller3_.inputs().test_bool_message());
    };

    void Step(const double &time_step) override {
        std::lock_guard<std::mutex> guard(mu_);
        outputs_->mutable_test_bool_message()->set_test(inputs().test_bool_message().test());
    }

};


class ControllerTestFixture : public Node {

public:

    const unsigned multi_rate_factor_ = 2; // see base constructor
    const unsigned multi_rate_factor_1_ = 1; // default if not assigned in constructor.
    const unsigned multi_rate_factor_2_ = 3;
    const unsigned multi_rate_factor_3_ = 5;

    TestExchangeDataTypes data_types_;

    ControllerDummy controller1_;
    ControllerDummy controller2_;
    ControllerDummy controller3_; // note: defined as sub model of controller 2 !

    ControllerTestFixture();

};

}  // namespace test
}  // namespace ocon
