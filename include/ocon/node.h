/*
 *
 * Copyright 2018 Peter de Lange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#pragma once

#include <ocon/node.pb.h>

#include <google/protobuf/message.h>
#include <google/protobuf/descriptor.h>

#include <vector>
#include <mutex>
#include <unordered_map>

namespace ocon {

/**
 * Virtual base class for OCON node
 *
 * An OCON node can either be a initialized as root (parent = nullptr) or as child
 *
 * Main (virtual) methods:
 *
 * - Init(): called during init execution
 * - Step(const double& dt): called during step execution
 * - InitCallback(): called during init execution after init execution of its children.
 * - StepCallback(): called during step execution after step execution of its children.
 *
 * Use RegisterInterface(Message*) to register and allocate interface messages
 * Use ConnectSignals(Message*, Message*) to connect messages between nodes
 */
class Node {
public:
    /// Constructor
    /// @param[in] parent Specifies parent node (set to null to make node it a root node)
    /// @param[in] name Name of the node. Note: children of the same parent must have unique names
    /// @param[in] multi_rate_factor Executes step for current node and children at multiple rate
    /// of the parent execution time step.
    Node(Node* parent, const std::string& name, const unsigned& multi_rate_factor = 1);
    Node(const Node&) = delete;
    Node& operator=(const Node&) = delete;
    virtual ~Node() = default;

    /// Executes step recursively (current node and all children)
    /// @param[in] time_step Time step used as per parent executor
    void ExecuteStep(const double& time_step);

    /// Executes init recursively (current node and all children)
    void ExecuteInit();

    /// Use this method to bind messages across different nodes within the same tree.
    /// Note: signals need to be registered and allocated first by using RegisterInterface(Message)
    /// @param[in] source Source message
    /// @param[in] target Target message
    /// @param[in] optimize Optimizes signal binding to ensure minimum latency.
    /// Disabling this flag makes the current node the owner and executor of
    /// the binding and exclude it as potential callback binding
    template<class TMessage>
    void ConnectSignals(const TMessage& source, const TMessage& target, bool optimize = true) {
        static_assert(std::is_base_of<google::protobuf::Message, TMessage>::value,
                "Source and target are not a message type");
        if (&TMessage::default_instance() == &source)
            throw std::runtime_error("Source is not an allocated interface.");
        if (&TMessage::default_instance() == &target)
            throw std::runtime_error("Target is not an allocated interface.");
        ConnectSignalsImpl(source, target, optimize);
    }

    /// Get node path
    /// @return current path of the node, e.g. "RootNode.Child2.SubChild1"
    std::string path() const;

    /// Get node message
    /// @param[in] node message request consiting of node path and message (interface) path
    /// @return Node interface message or empty any message if request was not found
    google::protobuf::Any message(const NodeMessageReference& request) const;

    /// Get node tree message
    /// @return Node tree message
    NodeTree tree() const;

    /// Get node info message
    /// @return Node info message
    NodeInfo info() const;

protected:
    /// Yes, you are getting the mutex.
    /// With great power comes great responsibility
    /// Node interfaces are all considered shared resources and should be guarded accordingly.
    mutable std::mutex mu_;

    /** Registering an interface allocates all sub messages and makes the interface and
     * all sub-messages eligible for signal binding.
     * Registered interfaces are also accessible using generic message requests
     * and are visible in the OCON web GUI
     *
     * @param[in] name Name of the interface
     * @param[in] interface Message pointer to class interface
     * @param[in] type Type of the interface, inputs, parameters, and so on.
     * This affects signal binding and saving / loading of parameters.
     */
    void RegisterInterface(const std::string &name, google::protobuf::Message* interface,
            NodeInterfaceType type = UNDEFINED);

private:
    // User definable (virtual) methods
    virtual void Init() {};
    virtual void Step(const double& time_step) {};
    virtual void InitCallback() {};
    virtual void StepCallback() {};

    // tree structure with flat node map for quick access.
    const Node* parent_ = nullptr;
    std::vector<Node*> children_;
    typedef std::map<std::string, Node*> node_map_t_;
    std::shared_ptr<node_map_t_> node_map_ = nullptr;

    // Node properties
    const std::string name_;
    const unsigned multi_rate_factor_;

    // Info message
    mutable std::mutex mu_info_;
    NodeInfo info_ = {};

    // Interfaces
    struct Interface {
        const std::string name;
        google::protobuf::Message* message;
        const NodeInterfaceType type;
    };
    std::vector<Interface> interfaces_;

    // Signal bindings
    typedef std::vector<const google::protobuf::FieldDescriptor*> fields_t_;
    struct SignalBindingData {
        SignalBinding message;
        std::pair<const Node*, const Node*> lock_order;
        Interface* source_interface;
        Interface* target_interface;
        fields_t_ source_fields;
        fields_t_ target_fields;
    };
    typedef std::vector<SignalBindingData> bindings_t_;
    bindings_t_ bindings_;
    bindings_t_ bindings_callback_;

    void ExecuteSignalBindings(const bindings_t_& bindings);

    void ConnectSignalsImpl(const google::protobuf::Message& source_message,
            const google::protobuf::Message& target_message, bool optimize);

    struct ParseMessageRequestResult {
        bool success = false;
        std::string error = std::string();
        Node* node = nullptr;
        google::protobuf::Message* message;
        std::string key;
        fields_t_ fields;
    };

    // Parse message request
    ParseMessageRequestResult ParseMessageRequest(const NodeMessageReference& request) const;

    struct SearchMessageResult {
        Node* node;
        Interface* interface;
        fields_t_ fields;
        NodeMessageReference reference;
    };

    // Auxiliary method used for connecting signals.
    // Searches for a message by address inside the entire controller tree
    // Returns data structure on success. Throws runtime errors on failure.
    SearchMessageResult SearchMessage(const google::protobuf::Message& message) const;

    // Find interface by name. returns null pointer when the interface could not be located.
    const Interface* FindInterface(const std::string& name) const;

    // Returns root node
    const Node* GetRoot() const;

    // Creates a node message reference message given a node, set of fields, and interface type.
    static NodeMessageReference CreateNodeMessageReference(const ocon::Node *node,
            const std::string& interface_name, const fields_t_ &fields);
};

}  // namespace ocon
