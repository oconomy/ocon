/*
 *
 * Copyright 2018 Peter de Lange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <chrono>
#include <math.h>

#include "pendulum.pb.h"

#include <ocon/node_template.h>
#include <ocon/server.h>

#include <chrono>
#include <thread>

using namespace ocon;
using namespace pendulum;

/* Equations of motion of a pendulum
 *
 *     .
 *     |\        g
 *     |_\ l     |
 *     |θ \
 *     |   o m
 *
 *   m : point mass [kg]
 *   l : length of pendulum [m]
 *   g : gravity [m/s^2]
 *   θ : angle [rad]
 *   u : torque [Nm]
 */
class Pendulum : public NodeTemplate<Inputs, State, google::protobuf::Empty, Parameters> {

public:
    explicit Pendulum(Node* parent, const std::string& name) : NodeTemplate(parent, name) {}

    void Init() override {
        std::lock_guard<std::mutex> guard(mu_);
        outputs_->Clear();
        parameters_->set_m(1.00);
        parameters_->set_g(9.81);
        parameters_->set_l(1.00);
    }

    void Step(const double& time_step) override {
        std::lock_guard<std::mutex> guard(mu_);

        // map in
        auto theta = outputs().theta();
        auto theta_dot = outputs().theta_dot();
        const auto& u = inputs().torque();
        const auto& g = parameters_->g();
        const auto& m = parameters_->m();
        const auto& l = parameters_->l();

        // state integration (simple 1st order Euler method)
        theta_dot += (-g/l*sin(theta) + u/(m*pow(l, 2.))) * time_step;
        theta += theta_dot * time_step;

        // map out
        outputs_->set_theta(theta);
        outputs_->set_theta_dot(theta_dot);
    }
};


int main () {
    // model
    Pendulum pendulum(nullptr, "MyPendulum");

    // server
    ServerConfiguration config;
    config.set_scheduler(true);
    config.set_time_step(0.01);
    Server server(&pendulum, config);

    // run for 10 seconds
    server.Run();
    std::this_thread::sleep_for(std::chrono::seconds(10));
}