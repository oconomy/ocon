# OCON pendulum example

Implements equations of motion of a pendulum. 
               
Have a look at the [pendulum.proto](pendulum.proto) file which shows the model interfaces and services.
               
## Build and run
Can be build as CMake subdirectory or standalone using the installed OCON CMake package. 

Make all targets
``` bash
mkdir -p cmake/build && cd cmake/build
cmake ../..  && make
```
Run the demo / main server:
``` bash
./pendulum
```