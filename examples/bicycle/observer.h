/*
 *
 * Copyright 2018 Peter de Lange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "bicycle.pb.h"

#include <ocon/node_template.h>

namespace bicycle {

class Observer : public ocon::NodeTemplate<ObserverInputs, ObserverOutputs, ObserverInternals, Empty> {
public:
    explicit Observer(Node* parent, const std::string& name);

private:
    void Init() override;
    void Step(const double& dt) override;
};

} // namespace bicycle
