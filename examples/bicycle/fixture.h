/*
 *
 * Copyright 2018 Peter de Lange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include "bicycle.pb.h"

#include "equations_of_motion.h"
#include "target.h"
#include "observer.h"

namespace bicycle {

/*! Bicycle root node
 *
 * Implements external interface (see proto) and consists of following children:
 *
 * - BicycleEOM: equations of motion of the bicycle
 * - TargetPath: periodical moving target (figure 8 shape) for learning path following
 * - Controller: outputs steer and pedal torque and can be programmed based on
 *               various input data consisting of the bicycle and target path states.
 */
class BicycleFixture final : public ocon::Node {
    google::protobuf::Arena arena_;
    ExternalInputs *external_inputs_;
    ExternalOutputs *external_outputs_;
    Target target_;
    BicycleEOM bicycle_;
    Observer observer_;

public:
    explicit BicycleFixture(const std::string &name, unsigned multi_rate_factor = 1);
    const ExternalInputs &external_inputs() const;
    const ExternalOutputs &external_outputs() const;
};

}  // namespace bicycle