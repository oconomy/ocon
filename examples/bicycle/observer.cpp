/*
 *
 * Copyright 2018 Peter de Lange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "observer.h"

#include <cmath>

namespace bicycle {

Observer::Observer(ocon::Node *parent, const std::string &name) : NodeTemplate(parent, name) {}

void Observer::Init() {
    std::lock_guard<std::mutex> guard(mu_);
    outputs_->Clear();
    internals_->Clear();
}

void Observer::Step(const double& dt) {
    std::lock_guard<std::mutex> guard(mu_);

    // calculate relative error between bicycle and target set point
    auto psi = inputs().bicycle().state().psi(); // heading angle of the bicycle
    auto psi_dot = inputs().bicycle().psi_dot(); // heading rate of the bicycle
    double delta_x = inputs().target().x() - inputs().bicycle().state().x(); // relative error in x direction
    double delta_y = inputs().target().y() - inputs().bicycle().state().y(); // relative error in y direction
    double delta_dxdt = inputs().target().dxdt() - inputs().bicycle().dxdt(); // relative error rate in x direction
    double delta_dydt = inputs().target().dydt() - inputs().bicycle().dydt(); // relative error rate in y direction
    double error = std::sqrt(std::pow(delta_x, 2.) + std::pow(delta_y, 2.)); // absolute error

    // output error conditions
    outputs_->set_error(error);
    outputs_->set_error_longitudinal(cos(psi)*delta_x + sin(psi)*delta_y);
    outputs_->set_error_longitudinal_rate(cos(psi)*delta_dxdt - psi_dot*sin(psi)*delta_x + sin(psi)*delta_dydt + psi_dot*cos(psi)*delta_y);
    outputs_->set_error_lateral(-sin(psi)*delta_x + cos(psi)*delta_y);
    outputs_->set_error_lateral_rate(-sin(psi)*delta_dxdt - psi_dot*cos(psi)*delta_x + cos(psi)*delta_dydt - psi_dot*sin(psi)*delta_y);

    // moving average of error
    double time = internals_->time();
    double error_time_average = outputs().error_time_average();
    error_time_average = (time * error_time_average + error * dt) / (time + dt);
    outputs_->set_error_time_average(error_time_average);

    // time
    internals_->set_time(time + dt);
}

} // using namespace bicycle;
