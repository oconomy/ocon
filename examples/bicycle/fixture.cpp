/*
 *
 * Copyright 2018 Peter de Lange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include "fixture.h"

namespace bicycle {

BicycleFixture::BicycleFixture(const std::string &name, unsigned multi_rate_factor) :
        Node(nullptr, "Bicycle", multi_rate_factor),
        external_inputs_(google::protobuf::Arena::CreateMessage<ExternalInputs>(&arena_)),
        external_outputs_(google::protobuf::Arena::CreateMessage<ExternalOutputs>(&arena_)),
        target_(this, "Target"),
        bicycle_(this, "BicycleEOM", 5),
        observer_(this, "Observer") {
    // Register external interfaces
    RegisterInterface("external_inputs", external_inputs_);
    RegisterInterface("external_outputs", external_outputs_);

    // Observer inputs
    ConnectSignals(bicycle_.outputs(), observer_.inputs().bicycle());
    ConnectSignals(target_.outputs(), observer_.inputs().target());

    // bicycle_fixture inputs
    ConnectSignals(external_inputs().forces(), bicycle_.inputs().forces());

    // Root node inputs
    ConnectSignals(target_.outputs(), external_outputs().target());
    ConnectSignals(bicycle_.outputs(), external_outputs().bicycle());
    ConnectSignals(observer_.outputs(), external_outputs().observer());
}

const ExternalInputs& BicycleFixture::external_inputs() const
{
    return *external_inputs_;
}

const ExternalOutputs& BicycleFixture::external_outputs() const
{
    return *external_outputs_;
}

}  // namespace bicycle
