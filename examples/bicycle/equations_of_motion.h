/*
 *
 * Copyright 2018 Peter de Lange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
# pragma once

#include "bicycle.pb.h"

#include <ocon/node_template.h>

#include <boost/numeric/odeint.hpp>
#include <Eigen/Dense>

namespace bicycle {

/*! Bicycle linearized equations of motion.
 *
 * Implemented bench mark bicycle EOM according to:
 * http://bicycle.tudelft.nl/schwab/Publications/06PA0459BicyclePaperv45.pdf
 * More information about bicycle dynamics; http://bicycle.tudelft.nl/schwab/Bicycle/
 *
 */
class BicycleEOM : public ocon::NodeTemplate<BicycleInputs, BicycleOutputs, Empty, BicycleParameters>
{
    typedef std::vector<double> v_double_t_;
    boost::numeric::odeint::runge_kutta4<v_double_t_> stepper_; // stepper for time integration of ODE.

public:
    explicit BicycleEOM(Node* parent, const std::string& name, unsigned multi_rate_factor = 1);

protected:
    // Inertial, damping, and stiffness matrices
    Eigen::Matrix2d M_;
    Eigen::Matrix2d K0_;
    Eigen::Matrix2d K2_;
    Eigen::Matrix2d C1_;

    Eigen::Matrix4d A_; // state space matrix A

private:
    void Init() override;
    void Step(const double& dt) override;
    void ODE(const v_double_t_ &x, v_double_t_ &dxdt, double t); // ordinary differential equation
};

} // namespace bicycle
