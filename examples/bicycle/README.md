# OCON bicycle example
Implements a OCON node tree consisting of 1 root node and 3 children:

 - BicycleEOM: equations of motion of the bicycle
 - Target: periodical moving target (figure 8 shape) for learning path following
 - Observer: outputs steer and pedal torque and can be programmed based on
               various input data consisting of the bicycle and target path states.
               
Have a look at the [bicycle.proto](bicycle.proto) file which shows the model interfaces and services.
               
## Build, test, and run
Can be build as CMake subdirectory or standalone using the installed OCON CMake package. 

Rquires the Eigen 3 C++ template library in addition to the OCON dependencies.

```bash
[sudo] apt install libeigen3-dev
```   

Make all targets
``` bash
mkdir -p cmake/build && cd cmake/build
cmake ../..  && make
```
Run the tester
``` bash
./bicycle_tester
```
Run the demo / main server:
``` bash
./bicycle_main
```