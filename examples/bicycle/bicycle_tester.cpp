/*
 *
 * Copyright 2018 Peter de Lange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "fixture.h"

#include <ocon/server.h>
#include <gtest/gtest.h>
#include <google/protobuf/util/message_differencer.h>

#include <Eigen/Eigenvalues>

class BicycleEOM_Fixture : public bicycle::BicycleEOM {
public:
    // public for testing purposes only
    double time_step = 0.005;
    BicycleEOM_Fixture(): bicycle::BicycleEOM(nullptr, "bicycle_fixture") {}
    
    void RunSeconds(double seconds) {
        for (unsigned i=0; i < seconds / time_step; i++) {
            ExecuteStep(time_step);
        }
    }

    auto eigenvalues () {
         Eigen::EigenSolver<Eigen::Matrix4d>e(A_, false);
         return e.eigenvalues();
    };
};

TEST(BicycleEOM, Eigenvalues) {

    BicycleEOM_Fixture f;
    f.mutable_inputs()->mutable_initial_state()->set_velocity(5.);

    f.ExecuteInit();
    f.ExecuteStep(0.0);

    EXPECT_NEAR(f.eigenvalues()[0].real(), -14.07838969279822, 1e-6);
    EXPECT_NEAR(f.eigenvalues()[0].imag(),   0.00000000000000, 1e-6);
    EXPECT_NEAR(f.eigenvalues()[1].real(),  -0.77534188219585, 1e-6);
    EXPECT_NEAR(f.eigenvalues()[1].imag(),   4.46486771378823, 1e-6);
    EXPECT_NEAR(f.eigenvalues()[1].real(),  -0.77534188219585, 1e-6);
    EXPECT_NEAR(f.eigenvalues()[2].imag(),  -4.46486771378823, 1e-6);
    EXPECT_NEAR(f.eigenvalues()[3].real(),  -0.32286642900409, 1e-6);
    EXPECT_NEAR(f.eigenvalues()[3].imag(),   0.00000000000000, 1e-6);
}

TEST(BicycleEOM, SteadyState) {
    BicycleEOM_Fixture f;
    f.mutable_inputs()->mutable_initial_state()->set_velocity(5.);
    f.ExecuteInit();
    ASSERT_NEAR(f.outputs().state().velocity(), f.inputs().initial_state().velocity(), 1e-6);

    ASSERT_NEAR(f.outputs().state().roll_angle(), 0., 1e-6);
    ASSERT_NEAR(f.outputs().state().steering_angle(), 0., 1e-6);

    f.RunSeconds(2.);

    ASSERT_NEAR(f.outputs().state().roll_angle(), 0., 1e-6);
    ASSERT_NEAR(f.outputs().state().steering_angle(), 0., 1e-6);
}

TEST(BicycleEOM, Perturbation) {

    BicycleEOM_Fixture f;
    f.mutable_inputs()->mutable_initial_state()->set_velocity(5.);
    f.ExecuteInit();

    // apply a small perturbation and observe
    ASSERT_NEAR(f.outputs().state().velocity(), f.inputs().initial_state().velocity(), 1e-6);
    f.mutable_inputs()->mutable_forces()->set_steer_torque(0.1);
    f.RunSeconds(2.);
    EXPECT_GT(abs(f.outputs().state().roll_angle()), 1e-2);
    EXPECT_GT(abs(f.outputs().state().steering_angle()), 1e-2);

    // remove torque and verify stability
    f.mutable_inputs()->mutable_forces()->set_steer_torque(0.0);
    f.RunSeconds(6.);
    ASSERT_NEAR(f.outputs().state().roll_angle(), 0., 1e-2);
    ASSERT_NEAR(f.outputs().state().steering_angle(), 0., 1e-2);
}
