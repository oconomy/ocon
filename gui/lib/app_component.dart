import 'package:angular/angular.dart';
import 'package:angular_components/app_layout/material_persistent_drawer.dart';
import 'package:angular_components/content/deferred_content.dart';
import 'package:angular_components/material_button/material_button.dart';
import 'package:angular_components/material_icon/material_icon.dart';
import 'package:angular_components/material_list/material_list.dart';
import 'package:angular_components/material_list/material_list_item.dart';
import 'package:angular_components/material_toggle/material_toggle.dart';

import 'package:ocon_gui/src/node_explorer_component.dart';
import 'package:ocon_gui/src/configuration_service.dart';


@Component(
  selector: 'my-app',
  styleUrls: ['package:angular_components/app_layout/layout.scss.css','app_component.css'],
  templateUrl: 'app_component.html',
  directives: [
    NodeExplorerComponent,
    DeferredContentDirective,
    MaterialButtonComponent,
    MaterialIconComponent,
    MaterialPersistentDrawerDirective,
    MaterialToggleComponent,
    MaterialListComponent,
    MaterialListItemComponent,
    NgFor,
    NgClass,
  ],
  providers: [
    const ClassProvider(ConfigurationService),
  ],
)
class AppComponent {

  final ConfigurationService _config_s;
  List<ServerConfig> get servers => _config_s.servers;
  set selected_server (ServerConfig server) => _config_s.selected_server = server;
  get selected_server  => _config_s.selected_server;
  AppComponent(this._config_s);


}