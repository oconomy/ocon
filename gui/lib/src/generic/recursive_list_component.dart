import 'package:angular/angular.dart';
import 'package:angular/core.dart' show Pipe, PipeTransform;

@Pipe('map_2_key_val')
class ValuesPipe extends PipeTransform {
  List<Map<String, dynamic>> transform(Map<String, dynamic> map) {
    List<Map<String, dynamic>> result = [];
    if (map != null) {
      map.forEach((k,v) {
        if (k != '@type') {
          if (v is Map) {
            result.add({'key': k, 'val': v, 'val_is_map': true});
          } else if (v is List) {
            List<String> keys = List<String>.generate(v.length, (i) => i.toString());
            result.add({'key': k, 'val': Map.fromIterables(keys, v), 'val_is_map': true});
          } else {
            result.add({'key': k, 'val': v, 'val_is_map': false}); // value
          }
        }
      });
    }
    return result;
  }
}

@Component(
  selector: 'recursive-list',
  template: '''
      <div> 
        <ul>
          <li *ngFor="let item of items | map_2_key_val; trackBy: trackByKey">
            {{item['key']}}<span *ngIf="!item['val_is_map']">: {{item['val']}}</span>
            <recursive-list [items]="item['val']" *ngIf="item['val_is_map']"></recursive-list>
          </li>
        </ul>
      </div>''',
  directives: [
    NgFor,
    NgIf,
    RecursiveList,
  ],
  pipes: [ValuesPipe],
)
class RecursiveList {
  @Input() Map<String, dynamic> items;
  Object trackByKey(int id, dynamic item) {
    return item['key'].hashCode;
  }
}
