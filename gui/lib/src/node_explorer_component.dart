import 'dart:async';

import 'package:angular/angular.dart';
import 'package:ocon_gui/src/generic/recursive_list_component.dart';
import 'package:ocon_gui/src/node_service.dart';
import 'package:ocon_gui/src/node_tree.dart';

@Component(
  selector: 'node-navigator',
  styleUrls: ['node_explorer_component.css'],
  template:
      '<div>'
        '<ul>'
          '<li (click)="onSelect(node)"><a class="link">{{node.info["name"]}}</a></li>'
            '<div *ngFor="let node_child of node.children; let i=index; trackBy: trackByKey">'
              '<node-navigator [node]="node_child" (selected_node)="onSelect(\$event)"></node-navigator>'
            '</div>'
        '</ul>'
    '</div>',
  directives: [
    NgFor,
    NgIf,
    NodeNavigator,
  ],
)
class NodeNavigator {
  @Input() NodeTree node;
  @Input() int index;

  final _sc = StreamController<NodeTree>();
  @Output() Stream<NodeTree> get selected_node => _sc.stream;

  Object trackByKey(int id, dynamic node_child) {
    return node_child.path.hashCode;
  }
  void onSelect(NodeTree node) {
    _sc.add(node);
  }
}

@Component(
  selector: 'node-explorer',
  styleUrls: ['node_explorer_component.css'],
  templateUrl: 'node_explorer_component.html',
  directives: [
    NgFor,
    NgIf,
    NodeNavigator,
    RecursiveList,
  ],
  providers: [
    const ClassProvider(NodeTreeService)
  ],
)
class NodeExplorerComponent implements OnInit {

  final NodeTreeService _node_tree_service;

  // node tree
  NodeTree _node_tree = null;
  NodeTree get node_tree => _node_tree;
  bool get node_tree_exists => _node_tree != null;
  List<NodeTree> get node_tree_list => [_node_tree];

  // selected node
  String _selected_node_path = '';
  NodeTree get selected_node {
    NodeTree result = node_tree.from_path(_selected_node_path);
    return result != null ? result : node_tree;
  }
  set selected_node(node) => _selected_node_path = node.path;

  NodeExplorerComponent(this._node_tree_service) {
  }

  @override
  Future<Null> ngOnInit() async {
    _node_tree_service.root_node_stream.listen((node_tree) {
      _node_tree = node_tree;
    });
  }

}
