import 'dart:async';
import 'dart:convert';
import 'dart:html';
import 'dart:core';
import 'package:angular/core.dart';
import 'package:ocon_gui/src/node_tree.dart';
import 'package:ocon_gui/src/configuration_service.dart';

import 'package:angular/angular.dart';


@Injectable()
class NodeTreeService {

  WebSocket _webSocket = null;
  final String _request = '';
  String _host;
  final ConfigurationService _config_s;

  // Set up full node stream
  final StreamController<NodeTree> _sc = new StreamController.broadcast();
  Stream<NodeTree> _node_tree_stream;
  Stream<NodeTree> get root_node_stream => _node_tree_stream;

  String create_host_address(ServerConfig server) {
    return 'ws://${server.host}:${server.port.toString()}/';
  }

  NodeTreeService(this._config_s) {
    _node_tree_stream = _sc.stream;
    _sc.add(null);
    _host = create_host_address(_config_s.selected_server);
    connect();
    _config_s.on_selection_change.listen((ServerConfig server) {
      _host = create_host_address(server);
      _webSocket.close();
      _webSocket = null;
      connect();
    });
    SendData();
    AutoReconnect();
  }

  void connect() {

    _webSocket = WebSocket(_host);
    _webSocket.onOpen.first.then((_) {
      onConnected();
        sendws();
      _webSocket.onClose.first.then((_) {
        print("Connection disconnected to ${_webSocket.url}");
        onDisconnected();
      });
    });
    _webSocket.onError.first.then((_) {
      print("Failed to connect to ${_webSocket.url}. "
          "Please make sure server is running and try again.");
      onDisconnected();
    });
  }

  void SendData() {
    if (_webSocket != null) {
      if (_webSocket.readyState == WebSocket.OPEN) {
        sendws();
      }
    }
    Timer(Duration(milliseconds:50), (){
      SendData();
    });
  }

  void AutoReconnect() {
    if (_webSocket != null) {
      if (_webSocket.readyState == WebSocket.CLOSED) {
        connect();
      }
    }
    Timer(Duration(seconds:1), (){
      AutoReconnect();
    });
  }

  void onConnected() {
    _webSocket.onMessage.listen((e) {
      onMessage(e.data);
    });
  }

  void onDisconnected() {
    _sc.add(null);
  }

  void onMessage(data) {
    final Map<String, dynamic> result = json.decode(data);
    Map<String, dynamic> response = result['node_tree'];
    response['server'] = result['server'];
    _sc.add(NodeTree(response));
  }

  void sendws(){
    _webSocket.send(_request);
  }

}
