import 'package:angular/angular.dart';
import 'package:angular/core.dart';
import 'dart:async';

import 'dart:core';
import 'dart:html';
import 'dart:convert';

class ServerConfig {

  final String name;
  final String host;
  final int port;

  ServerConfig(this.name, this.host, [this.port = 8080]) {
  }

}

/// Mock service emulating access to a to-do list stored on a server.
@Injectable()
class ConfigurationService {

  StreamController<ServerConfig> selection_change = new StreamController.broadcast();
  Stream<ServerConfig> get on_selection_change => selection_change.stream;

  final List<ServerConfig> _servers = [];
  List<ServerConfig> get servers => _servers;


  ServerConfig _selected_server;
  ServerConfig get selected_server => _selected_server;
  set selected_server(ServerConfig selected) {
    _selected_server = selected;
    selection_change.add(selected);
  }

  ConfigurationService() {
      _servers.add(ServerConfig("localhost","127.0.0.1"));
      _selected_server = _servers[0];
      HttpRequest.getString('config.json').then((myjson) {
        Map data = json.decode(myjson);
        List<dynamic> nodes = data["nodes"];
        nodes.forEach((node) {
          _servers.add(ServerConfig(node["name"], node["ip"]));
        });
      });

  }
}
