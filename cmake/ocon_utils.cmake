find_package(Protobuf 3.0.0 REQUIRED)

set(PROTOBUF_PROTOC $<TARGET_FILE:protobuf::protoc>)

function(ocon_protobuf_generate_cpp PROTO_PATH PROTO_SRCS_NAME PROTO_HDRS_NAME [PROTO_FILES])
    set(PROTO_SRCS)
    set(PROTO_HDRS)
    foreach(PROTO_FILE ${PROTO_FILES})
        get_filename_component(PROTO_ABS_PATH "${PROTO_PATH}" ABSOLUTE)
        get_filename_component(PROTO ${PROTO_ABS_PATH}/${PROTO_FILE} ABSOLUTE)
        get_filename_component(PROTO_DIRECTORY ${PROTO_FILE} DIRECTORY )
        get_filename_component(PROTO_NAME_WE ${PROTO_FILE} NAME_WE)
        set(PROTO_SRC "${CMAKE_CURRENT_BINARY_DIR}/${PROTO_DIRECTORY}/${PROTO_NAME_WE}.pb.cc")
        set(PROTO_HDR "${CMAKE_CURRENT_BINARY_DIR}/${PROTO_DIRECTORY}/${PROTO_NAME_WE}.pb.h")
        list(APPEND PROTO_SRCS "${PROTO_SRC}")
        list(APPEND PROTO_HDRS "${PROTO_HDR}")
        add_custom_command(
            OUTPUT "${PROTO_SRC}" "${PROTO_HDR}"
            COMMAND ${PROTOBUF_PROTOC}
            ARGS
                --cpp_out "${CMAKE_CURRENT_BINARY_DIR}"
                --proto_path "${PROTO_ABS_PATH}"
                -I${PROJECT_SOURCE_DIR}
                ${PROTO}
            DEPENDS ${PROTO}
        )
    endforeach()
    set(${PROTO_SRCS_NAME} ${PROTO_SRCS} PARENT_SCOPE)
    set(${PROTO_HDRS_NAME} ${PROTO_HDRS} PARENT_SCOPE)
endfunction()