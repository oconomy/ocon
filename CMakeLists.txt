cmake_minimum_required(VERSION 3.7)

set(PACKAGE_NAME      "ocon")
set(PACKAGE_VERSION   "0.1.0")
set(PACKAGE_STRING    "${PACKAGE_NAME} ${PACKAGE_VERSION}")
set(PACKAGE_TARNAME   "${PACKAGE_NAME}-${PACKAGE_VERSION}")

project(${PACKAGE_NAME} VERSION ${PACKAGE_VERSION} LANGUAGES CXX )

set(ocon_INSTALL_BINDIR "bin" CACHE STRING "Installation directory for executables")
set(ocon_INSTALL_LIBDIR "lib" CACHE STRING "Installation directory for libraries")
set(ocon_INSTALL_INCLUDEDIR "include/${PACKAGE_NAME}" CACHE STRING "Installation directory for headers")
set(ocon_INSTALL_CMAKEDIR "lib/cmake/${PACKAGE_NAME}" CACHE STRING "Installation directory for cmake config files")

# Options
option(ocon_BUILD_TESTS "Build tests" TRUE)
option(ocon_BUILD_EXAMPLE "Build example" TRUE)

# Package dependencies
find_package(Protobuf 3.0.0 REQUIRED)
set(Boost_USE_STATIC_LIBS ON)
set(Boost_USE_STATIC_RUNTIME ON)
find_package(Boost 1.70.0 REQUIRED )

# Make dependencies
include(cmake/ocon_utils.cmake) # ocon_generate_cpp_proto

# generate proto source and header files
set(PROTO_FILES ocon/node.proto ocon/server.proto)
ocon_protobuf_generate_cpp(${PROJECT_SOURCE_DIR}/proto PROTO_SRCS PROTO_HDRS ${PROTO_FILES})

message("${PROTO_SRCS}")
message("${PROTO_HDRS}")
message("${CMAKE_CURRENT_BINARY_DIR}")

# Target
add_library(ocon STATIC
        ${PROTO_SRCS} ${PROTO_HDRS}
        src/node.cpp src/utils.cpp src/server.cpp src/server_websocket.cpp)
target_include_directories(ocon
        PUBLIC $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include> $<INSTALL_INTERFACE:include>
        PUBLIC $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}> $<INSTALL_INTERFACE:include>
        PRIVATE ${PROJECT_SOURCE_DIR}/src ${Boost_INCLUDE_DIRS})
target_link_libraries(ocon
        PUBLIC protobuf::libprotobuf
        PRIVATE ${Boost_LIBRARIES} pthread)
set_target_properties(ocon PROPERTIES CXX_STANDARD 14 POSITION_INDEPENDENT_CODE TRUE)

# Package configuration
set(TARGETS_EXPORT_NAME ${PROJECT_NAME}_targets)
set(generated_dir "${CMAKE_CURRENT_BINARY_DIR}/generated")
set(version_config "${generated_dir}/${PROJECT_NAME}ConfigVersion.cmake")
set(project_config "${generated_dir}/${PROJECT_NAME}Config.cmake")
include(CMakePackageConfigHelpers)
write_basic_package_version_file(
        "${version_config}" COMPATIBILITY SameMajorVersion
)

configure_package_config_file(
        "${PROJECT_SOURCE_DIR}/cmake/Config.cmake.in"
        "${project_config}"
        INSTALL_DESTINATION "${ocon_INSTALL_CMAKEDIR}"
)

# Install
install(TARGETS ocon EXPORT ${TARGETS_EXPORT_NAME}
        LIBRARY DESTINATION ${ocon_INSTALL_LIBDIR}
        ARCHIVE DESTINATION ${ocon_INSTALL_LIBDIR}
        RUNTIME DESTINATION ${ocon_INSTALL_BINDIR}
        INCLUDES DESTINATION ${ocon_INSTALL_INCLUDEDIR})
install(DIRECTORY ${PROJECT_SOURCE_DIR}/include/ocon/
        DESTINATION ${ocon_INSTALL_INCLUDEDIR}
        FILES_MATCHING PATTERN "*.h")
install(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/ocon/
        DESTINATION ${ocon_INSTALL_INCLUDEDIR}
        FILES_MATCHING PATTERN "*.h")
install(DIRECTORY ${PROJECT_SOURCE_DIR}/proto/
        DESTINATION ${ocon_INSTALL_INCLUDEDIR}
        FILES_MATCHING PATTERN "*.proto"
        PATTERN "CMakeFiles" EXCLUDE)
install(FILES "${project_config}" "${version_config}" "${PROJECT_SOURCE_DIR}/cmake/ocon_utils.cmake"
        DESTINATION ${ocon_INSTALL_CMAKEDIR})
install(EXPORT ${TARGETS_EXPORT_NAME}
        NAMESPACE "ocon::"
        DESTINATION ${ocon_INSTALL_CMAKEDIR})

# Unit tests
find_package(GTest REQUIRED)
add_subdirectory(test)

# Examples
set(OCON_AS_SUBMODULE TRUE)
add_library(ocon::ocon ALIAS ocon)
add_subdirectory(examples EXCLUDE_FROM_ALL)